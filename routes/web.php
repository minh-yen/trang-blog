<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// bai tap ve middleware
/*Route::get('/welcome', function(){
	
	return view('welcome');
});
Route::get('login', function(){
	return view('login');
});*/




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/index','PostController@index')->name('list_posts');


Route::get('/create','PostController@viewsCreate')->name('create');
Route::get('show_post/{id}','PostController@show')->name('show_post');
Route::post('/create_posts','PostController@create')->name('create_posts');
Route::get('/edit_posts/{id}','PostController@edit')->name('edit_posts');
Route::get('/delete_post/{id}','PostController@destroy')->name('delete_post');
Route::post('/update/{id}','PostController@update')->name('update_posts');

Route::post('/comment/{id}', 'PostController@comment')->name('comment');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// tạo login
Route::get('/','UserController@getLogin')->name('get_login');
Route::post('/login_blog','UserController@postLogin')->name('login_blog');
//tao register
Route::get('/register', 'RegisterController@getRegister')->name('register');
Route::post('/register_blog', 'RegisterController@postRegister')->name('register_blog');

Route::get('logout_blog', 'UserController@logout')->name('logout_blog');