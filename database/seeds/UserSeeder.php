<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
                'email' => 'lxc150896@gmail.com',
                'password' => Hash::make('12345'),
            ],
            [
                'email' => 'lxc@gmail.com',
                'password' => Hash::make('12345'),
            ],
            [
                'email' => 'admin@gmail.com',
                'password' => Hash::make('12345'),
            ],
        ];
        DB::table('loyal_customers')->insert($data);
    }
}
