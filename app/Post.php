<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $tables = 'posts';
    protected $fillable = ['id','title','url','content','status'];

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
