<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $tables = 'comments';
    protected $fillable = ['id','content','post_id'];
}
