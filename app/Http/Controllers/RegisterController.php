<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator ;
use App\LoyalCustomer ;
use Auth ;
use Hash;
class RegisterController extends Controller
{
	public function validator(array $data) {
		return Validator::make($data,[
			'email' => 'required|email|max:255!unique:loyal_customers',
			'password' => 'required|confirmed|min:5',
	]);
	}

	public function create(array $data) {
		return LoyalCustomer::create([
			'email'=>$data['email'],
			'password' => Hash::make($data['password']),
		]);
	}

    public function getRegister() {
    	return view('register');
    }

    public function postRegister(Request $request) {
    	$validator = $this->validator($request->all());
    	// dd($validator);

    	if($validator->fails()) {
    		
    		return redirect()->back()->withErrors($validator)->withInput($request->all());
    	}
    	$loyal_customer = $this->create($request->all());

    	Auth::login($loyal_customer);
    	return redirect()->route('list_posts');
    }
}
