<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Post;
use App\Comment;
use Illuminate\Support\Carbon;
class PostController extends Controller
{

    public function __construct(){
        $this->middleware('auth:loyal_customer')->except('logout_blog');
    }

    public function index() {

        $posts = Post::with('comments')->get();
        return view('content_blog' ,compact('posts'));
    }

	public function viewsCreate() {
		return view('create');
	}

    public function create(Request $request) {
    	
    	$rules = [
    		'title'=>'required',
    		'url'=>'required',
    		'content'=>'required',
    		'status'=>'required'
    	];
    	$messages = [
    		'title.required'=>'ban phai nhap tieu de',
    		'url.required'=>'ban phai nhap url',
    		
    		'content.required'=>'ban phai nhap content',
    		'status.required'=>'ban phai nhap status'
    	];

    	$validator = Validator::make($request->all(), $rules, $messages);
    	if($validator->fails()){
    		return redirect()->back()->withErrors($validator);	
    	}
    	$posts_create = Post::create($request->all());
    	if($posts_create) {
    		return redirect()->route('list_posts') ;
    	}

    	return redirect()->route('create')->with('status', 'tạo mới không thành công!') ;
    }

    public function show($id) {
        $posts_show = Post::select('posts.*')
                        ->where('id', $id)
                        ->with('comments')
                        ->first();

        if($posts_show) {
            return view('show',compact('posts_show'));
        }
        else {
            return redirect()->route('show_post')->with('status', 'id ban nhap khong ton');
        }
    }
    public function edit($id){
        $show = Post::select( 'id','title', 'url', 'content', 'status')->where('id', $id)->first();
        if($show) {
            return view('edit', compact('show')) ;
        }

    }
    public function update(Request $request, $id){
        $rules = [
            'title'=>'required',
            'url'=>'required',
            'content'=>'required',
            'status'=>'required'
        ];
        $messages = [
            'title.required'=>'ban phai nhap tieu de',
            'url.required'=>'ban phai nhap url',
            
            'content.required'=>'ban phai nhap content',
            'status.required'=>'ban phai nhap status'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);  
        }
        $posts_update = Post::where('id',$id)->update([
                'title'=> $request->title,
                'url'=> $request->url,
                'content'=>$request->content,
                'status'=>$request->status
        ]);
        if($posts_update) {
            return redirect()->route('list_posts')->with('success','sua thanh cong') ;
        }

        return redirect()->route('edit_posts')->with('errors', 'sua không thành công!') ;
    }

    public function destroy($id){
        $delete = Post::where('id', $id)->delete();

        if ($delete) {
            return redirect()->route('list_posts');
        } else {
            return redirect()->back();
        }
    }

    public function comment (Request $request, $id){
        
        if ($request->content == null) {
            return redirect()->back();
        }

        Comment::create([
            'content' => $request->content,
            'post_id' => $id
        ]);

        return redirect()->route('list_posts');
    }

    // tao api 
   /* public function check() 
    {
        logger('abac');
        return response()->json(['data' => 'ok']);
    }*/
}
