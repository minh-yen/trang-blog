<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    public function getLogin() {
    	return view('login');
    }
    public function postLogin(Request $request)
    {

        $arr = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        if ($request->remember == trans('remember.Remember Me')) {
            $remember = true;
        } else {
            $remember = false;
        }
        //kiểm tra trường remember có được chọn hay không
       
        if (Auth::guard('loyal_customer')->attempt($arr)) {

            return redirect()->route('list_posts');
            
            //đăng nhập thành công thì hiển thị thông báo đăng nhập thành công
        } else {

            return redirect()->back()->with('mess','Email va password ban nhap k dung! Moi nhap lai');
           
            //đăng nhập thất bại hiển thị đăng nhập thất bại
        }
    }

    public function logout(){
    	Auth::guard('loyal_customer')->logout();

    	return redirect()->route('get_login');
    }
}
