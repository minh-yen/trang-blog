<?php

namespace App\Http\Middleware;

use Closure;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*if(Auth::guard($guard)->guest()) {
            if($request->ajax()|| $request->wantsJson()) {
                return response('Unauthorized', 401);
            }
            return redirect()->guest('login');
        }
        return $next($request);
    }*/
    
        if(!$request->header('token')) {
            return response()->json(['data'=>'loi'], 403);
        }
        return $next($request);

    }
}
