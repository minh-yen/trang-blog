<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Create posts</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
	
	@if (session('errors'))
		<div class="alert alert-danger">
			<strong>{{ session('errors') }}</strong>
		</div>
	@endif
	
	<form action="{{ route('create_posts') }}" method="POST" role="form">
		@csrf
		<legend>Form title</legend>
		
		<div class="form-group">
			<label for="">title</label>
			<input type="text" class="form-control" id="" name="title" placeholder="Input field">
		</div>
		@if ($errors->has('title'))
			<p class="help text-danger">{!! $errors->first('title') !!}</p>
		@endif

		<div class="form-group">
			<label for="">url</label>
			<input type="text" class="form-control" id="" name="url" placeholder="Input field">
		</div>
		@if ($errors->has('url'))
			<p class="help text-danger">{!! $errors->first('url') !!}</p>
		@endif
		

		<div class="form-group">
			<label for="">content</label>
			<input type="text" class="form-control" id="" name="content" placeholder="Input field">
		</div>
		@if ($errors->has('content'))
			<p class="help text-danger">{!! $errors->first('content') !!}</p>
		@endif

		<div class="form-group">
			<label for="">status</label>
			<input type="text" class="form-control" id="" name="status" placeholder="Input field">
		</div>
		@if ($errors->has('status'))
			<p class="help text-danger">{!! $errors->first('status') !!}</p>
		@endif
		<br>
		<button type="submit" name="submit" class="btn btn-primary">Submit</button>
	</form>
</body>
</html>