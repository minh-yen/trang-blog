<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Tada & Blog - Personal Blog HTML Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
    <!-- STYLES -->
    <link rel="stylesheet" type="text/css" href="asset/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="asset/css/slippry.css">
    <link rel="stylesheet" type="text/css" href="asset/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="asset/css/style.css">
    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Sarina' rel='stylesheet' type='text/css'>

    <style type="text/css">
        .myBtn {
            padding: 8px 24px;
            top: 25px;
            left: 50%;
            transform: translate(-50%, 0);
            text-transform: uppercase;
            border-radius: 5px;
            box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.4);
            font-size: 12px;
            line-height: 1;
            background: #9c8156;
            color: white;
        }

        .myBtn:hover {
            color: white;
        }

        .myIP {
            border: 0px;
            border-bottom: 2px solid lightgray;
            font-style: italic;
            font-size: 12px;
            width: 100%;
        }
    </style>
</head>

<body>


    <!-- *****************************************************************
    ** Preloader *********************************************************
    ****************************************************************** -->

	<div id="preloader-container">
    	<div id="preloader-wrap">
    		<div id="preloader"></div>
    	</div>
    </div>

    
    <!-- *****************************************************************
    ** Header ************************************************************ 
    ****************************************************************** --> 

    <header class="tada-container">
    
    
    	<!-- LOGO -->    
    	@include('header')
        @include('sidebar_blog')
        
        <!-- SLIDER -->
        <!-- #SLIDER -->
                
                
    </header><!-- #HEADER -->

    
    <!-- *****************************************************************
    ** Section ***********************************************************
    ****************************************************************** -->
    
	<section class="tada-container content-posts">
    
    
    	<!-- CONTENT -->
        @yield('content_blog')
    	 <!-- #SIDEBAR -->
        
   		
        
        
    </section>

    
    <!-- *****************************************************************
    ** Footer ************************************************************
    ****************************************************************** -->    
    
    @include('footer_blog')
    
    
    <!-- *****************************************************************
    ** Script ************************************************************
    ****************************************************************** -->	
	
	<script src="asset/js/jquery-1.12.4.min.js"></script>
	<script src="asset/js/slippry.js"></script>
    <script src="asset/js/main.js"></script>

</body>
</html>
