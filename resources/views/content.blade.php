@extends('layout')

@section('content')
<div class="container" style="margin-top:30px">
  <div class="row">
    <div class="col-sm-4">
      <h2>About Me</h2>
      <h5>Photo of me:</h5>
      <div class="fakeimg">Fake Image</div>
      <p>Some text about me in culpa qui officia deserunt mollit anim..</p>
      <h3>Some Links</h3>
      <p>Lorem ipsum dolor sit ame.</p>
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a class="nav-link active" href="#">Active</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li>
      </ul>
      <hr class="d-sm-none">
    </div>
    <div class="col-sm-8">
      @if (session('success'))
      <div class="alert alert-success">
        <strong>{{ session('success') }}</strong>
      </div>
      @endif
      @if (isset($posts))
      
      @foreach ($posts as $post)
      <div class="card mb-5">
        <a class="card-header" href="{{$post->url}}" > {{$post->title}} </a>
        <div class="card-body">
          <img src="{{ asset('img/meo.jpg') }}" class="img-responsive post-img card-img" alt="Image">
          <p class="card-text"> {{$post->content}} </p>
          <a href="{{ route('show_post', $post->id) }}">See More</a>
        </div>
      </div>
      
      @endforeach
      @endif
      
    </div>
  </div>
</div>


@endsection 