<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
	@if (session('status'))
	<div class="alert alert-danger">
		<strong>{{ session('status') }}</strong>
	</div>
	@endif
	<table class="table table-hover">
		<thead>
			<tr>
				<th>id</th>
				<th>title</th>
				<th>url</th>
				<th>content</th>
				<th>status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			
			@if(isset($posts_show)) 
			
			<tr>
				<td>{{$posts_show->id}}</td>
				<td>{{$posts_show->title}}</td>
				<td>{{$posts_show->url}}</td>
				<td>{{$posts_show->content}}</td>
				<td>{{$posts_show->status}}</td>
				<td>
					<a href="{{ route('list_posts') }}">Quay lai</a>
					<a href="{{ route('edit_posts',$posts_show->id)}}">Sua</a>
				</td>
			</tr>
			<br>
			<br>
			<br>
			<br>

		</tbody>
	</table>
	@foreach ($posts_show->comments as $key => $comment)

	<p>Comment {{ $key + 1 }}: {{ $comment->content }}</p>
	@endforeach
	@endif
</body>
</html>